﻿using System;

namespace QuadPrecision
{
	public struct Quadruple
	{
        private static Double Split = (Double) (1 + (1 << 27)); // 134217729.0;

		private Double _hi;
		private Double _lo;

		private Quadruple(Double hi, Double lo)
		{
			_hi = hi + lo;
			_lo = lo + (hi - _hi);
		}

		public Quadruple(Quadruple value) : this(value._hi, value._lo)
		{
		}

		public Quadruple(Double value)
		{
			_hi = value;
			_lo = 0.0;
		}

		public static Quadruple operator +(Quadruple value, Quadruple addend)
		{
			value.Add(addend);
			return value;
		}

		public static Quadruple operator +(Quadruple value, Double addend)
		{
			value.Add(addend);
			return value;
		}

		public static Quadruple operator +(Double value, Quadruple addend)
		{
			addend.Add(value);
			return addend;
		}

		public static Quadruple operator -(Quadruple value, Quadruple subtrahend)
		{
			value.Subtract(subtrahend);
			return value;
		}

		public static Quadruple operator -(Quadruple value, Double subtrahend)
		{
			value.Add(-subtrahend);
			return value;
		}

		public static Quadruple operator -(Double value, Quadruple subtrahend)
		{
			subtrahend.Negate();
			subtrahend.Add(value);
			return subtrahend;
		}

		public static Quadruple operator *(Quadruple value, Quadruple multiplier)
		{
			value.MultiplyBy(multiplier);
			return value;
		}

		public static Quadruple operator *(Quadruple value, Double multiplier)
		{
			value.MultiplyBy(multiplier);
			return value;
		}

		public static Quadruple operator *(Double value, Quadruple multiplier)
		{
			multiplier.MultiplyBy(value);
			return multiplier;
		}

		public static Quadruple operator /(Quadruple value, Quadruple divisor)
		{
			value.DivideBy(divisor);
			return value;
		}

		public static Quadruple operator /(Quadruple value, Double divisor)
		{
			value.DivideBy(divisor);
			return value;
		}

		public static Quadruple operator /(Double value, Quadruple divisor)
		{
			var quad = new Quadruple(value);
			quad.DivideBy(divisor);
			return quad;
		}

        public static Boolean operator <(Quadruple value, Quadruple comparator) => value.Compare(comparator) < 0;
        public static Boolean operator >(Quadruple value, Quadruple comparator) => value.Compare(comparator) > 0;
        public static Boolean operator <=(Quadruple value, Quadruple comparator) => value.Compare(comparator) <= 0;
        public static Boolean operator >=(Quadruple value, Quadruple comparator) => value.Compare(comparator) >= 0;
        public static Boolean operator ==(Quadruple value, Quadruple comparator) => value.Compare(comparator) == 0;
        public static Boolean operator !=(Quadruple value, Quadruple comparator) => value.Compare(comparator) != 0;
        public static Boolean operator <(Quadruple value, Double comparator) => value.Compare(comparator) < 0;
        public static Boolean operator >(Quadruple value, Double comparator) => value.Compare(comparator) > 0;
        public static Boolean operator <=(Quadruple value, Double comparator) => value.Compare(comparator) <= 0;
        public static Boolean operator >=(Quadruple value, Double comparator) => value.Compare(comparator) >= 0;
        public static Boolean operator ==(Quadruple value, Double comparator) => value.Compare(comparator) == 0;
        public static Boolean operator !=(Quadruple value, Double comparator) => value.Compare(comparator) != 0;

        public static Quadruple Parse(String valueString)
		{
			var result = new Quadruple();
			Int32 n, sign, ex = 0;
			Int32 i = 0;

			// eat whitespace
			while (valueString[i] == ' ' || valueString[i] == '\t' || valueString[i] == '\n')
				i++;

			switch (valueString[i])
			{ // get sign of mantissa
				case '-':
					sign = -1;
					i++;
					break;

				case '+':
					i++;
					sign = 1;
					break;

				default:
					sign = 1;
					break;
			}

			// get digits before decimal point
            while (i < valueString.Length)
			{
				n = valueString[i] - '0';
				i++;
				if (n < 0 || n > 9) break;
				result.MultiplyBy(10.0);
				result.Add((Double)n);
			}

			i--;
			if (valueString[i] == '.')
			{ // get digits after decimal point
				i++;
                while (i < valueString.Length)
				{
					n = valueString[i] - '0';
					i++;
					if (n < 0 || n > 9) break;
					result.MultiplyBy(10.0);
					result.Add((Double)n);
					--ex;
				}
				i--;
			}

			// get exponent
			if (valueString[i] == 'e' || valueString[i] == 'E')
			{
				i++;
				ex += Int32.Parse(valueString.Substring(i));
			}
			// exponent adjustment
			while (ex-- > 0)
				result.MultiplyBy(10.0);
			while (++ex < 0)
				result.DivideBy(10.0);

			if (sign < 0)
				result.Negate();

			result.Normalize();
			return result;
		}

        public override String ToString()
        {
            return "";
        }

		private void Add(Quadruple addend)
		{
			Double S = _hi + addend._hi;
			Double T = _lo + addend._lo;
			Double e = S - _hi;
			Double f = T - _lo;
			Double s = S - e;
			Double t = T - f;
			s = (addend._hi - e) + (_hi - s);
			t = (addend._lo - f) + (_lo - t);
			e = s + T;
			Double H = S + e;
			Double h = e + (S - H);
			e = t + h;
			_hi = H + e;
			_lo = e + (H - _hi);
		}

		private void Add(Double addend)
		{
			Double S = addend + _hi;
			Double e = S - addend;
			Double s = S - e;
			s = (_hi - e) + (addend - s);
			Double H = S + (s + _lo);
			Double h = (s + _lo) + (S - H);
			_hi = H + h;
			_lo = h + (H - _hi);
		}

		private void Subtract(Quadruple subtrahend)
		{
			var addend = new Quadruple(subtrahend);
			addend.Negate();
			Add(addend);
		}

		private void Negate()
		{
			_hi = -_hi;
			_lo = -_lo;
			Normalize();
		}

		private void MultiplyBy(Quadruple multiplier)
		{
			Double C = Split * _hi;
			Double hx = C - _hi;
			Double c = Split * multiplier._hi;
			hx = C - hx;
			Double tx = _hi - hx;
			Double hy = c - multiplier._hi;
			C = _hi * multiplier._hi;
			hy = c - hy;
			Double ty = multiplier._hi - hy;
			c = ((((hx * hy - C) + hx * ty) + tx * hy) + tx * ty) + (_hi * multiplier._lo + _lo * multiplier._hi);
			_hi = C + c;
			hx = C - _hi;
			_lo = c + hx;
		}

		private void MultiplyBy(Double multiplier)
		{
			Double C = Split * multiplier;
			Double hx = C - multiplier;
			Double c = Split * _hi;
			hx = C - hx;
			Double tx = multiplier - hx;
			Double hy = c - _hi;
			C = multiplier * _hi;
			hy = c - hy;
			Double ty = _hi - hy;
			c = ((((hx * hy - C) + hx * ty) + tx * hy) + tx * ty) + multiplier * _lo;
			_hi = C + c;
			_lo = c + (C - _hi);
		}

		private void DivideBy(Quadruple divisor)
		{
			Double C = _hi / divisor._hi;
			Double c = Split * C;
			Double hc = c - C;
			Double u = Split * divisor._hi;
			hc = c - hc;
			Double tc = C - hc;
			Double hy = u - divisor._hi;
			Double U = C * divisor._hi;
			hy = u - hy;
			Double ty = divisor._hi - hy;
			u = (((hc * hy - U) + hc * ty) + tc * hy) + tc * ty;
			c = ((((_hi - U) - u) + _lo) - C * divisor._lo) / divisor._hi;
			_hi = C + c;
			_lo = (C - _hi) + c;
		}

		private void DivideBy(Double divisor)
		{
			Double C = _hi / divisor;
			Double c = Split * C;
			Double hc = c - C;
			Double u = Split * divisor;
			hc = c - hc;
			Double tc = C - hc;
			Double hy = u - divisor;
			Double U = C * divisor;
			hy = u - hy;
			Double ty = divisor - hy;
			u = (((hc * hy - U) + hc * ty) + tc * hy) + tc * ty;
			c = (((_hi - U) - u) + _lo) / divisor;
			_hi = C + c;
			_lo = (C - _hi) + c;
		}

		private void Normalize()
		{
			Double hi = _hi;
			Double lo = _lo;
			_hi = hi + lo;
			_lo = lo + (hi - _hi);
		}

		// do this for operator comparisons - not a useful comment
		private Int32 Compare(Quadruple comparator)
		{
            if (_hi > comparator._hi) return 1;
            if (_hi < comparator._hi) return -1;
            if (_lo > comparator._lo) return 1;
            if (_lo < comparator._lo) return -1;
            return 0;
		}

		private Int32 Compare(Double comparator)
		{
            return Compare(new Quadruple(comparator));
		}

        public override bool Equals(object obj)
        {
            if (!(obj is Quadruple))
            {
                return false;
            }

            var quadruple = (Quadruple)obj;
            return Math.Abs(_hi - quadruple._hi) < Double.Epsilon && Math.Abs(_lo - quadruple._lo) < Double.Epsilon;
        }

        public override int GetHashCode()
        {
            var hashCode = -387359596;
            hashCode = hashCode * -1521134295 + _hi.GetHashCode();
            hashCode = hashCode * -1521134295 + _lo.GetHashCode();
            return hashCode;
        }
    }
}
